import { actionTypes } from "../actionTypes/actionTypes";

/**
 * 
 * @param {*} state (default empty)
 * @param {*} action 
 * @returns the authentication state
 */
export const authReducer = (state = {}, action) => {

    switch (action.type) {
        case actionTypes.login:
            
            return {
                uid: action.payload.uid,
                name: action.payload.displayName,
                image: action.payload.photoURL,
                email: action.payload.email,
                token: action.payload.token,
                refreshToken: action.payload.refreshToken
            };
    
        case actionTypes.logout:
            return {};

        case actionTypes.auth:
            return {
                ...state,
                token: action.payload.token,
                refreshToken: action.payload.refreshToken
            };
        
        case actionTypes.saveCredential:
            return {
                ...state,
                credential: action.payload.credential
            }

        case actionTypes.checkUser:
            return state;

        default:
            return state;
    }
}