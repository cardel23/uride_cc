import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import thunk from "redux-thunk";
import { authReducer } from "../reducers/authReducer";
import { uiReducer } from "../reducers/uiReducer";


/**
 * cuando hay nuevas funcionalidades se agregan aqui
 */
const reducers = combineReducers({
    auth: authReducer,
    ui: uiReducer,
});


/**
 * Permite agregar varios middlewares
 */
const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

/**
 * Create store solo recibe un reducer
 */

export const store = createStore(reducers,
    composeEnhancers(
        /**
         * thunk es un middleware para manejar peticiones asincronas
         */
        applyMiddleware(thunk)
    )
);