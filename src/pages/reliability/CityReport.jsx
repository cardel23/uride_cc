import React from 'react'
import Typography from '@material-ui/core/Typography';
import { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Card from "@material-ui/core/Card";
import Chip from "@material-ui/core/Chip";
import "./reliability.css";
import { Button, ButtonGroup, CardActions, CardContent, IconButton, Dialog } from '@material-ui/core';
import { Apps, List, CalendarToday, ArrowUpward, ArrowDownward } from "@material-ui/icons";
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { useEffect } from 'react';
import Main from '../../components/main/Main';
import InfiniteTable from './InfiniteTable';
import axios from 'axios';

import { Calendar } from '../../components/dateRangePicker/Calendar';


export default function CityReport(props) {

    const [grid, setGrid] = useState(false);
    
    const handleGrid = () => setGrid(true);
    const handleList = () => setGrid(false);
    
    const [drivers, setDrivers] = useState([]);

    const [city, setCity] = useState(props.city);

    /**
     * For pagination control
     */
    const [page, setPage] = useState(0);
    const [pages, setPages] = useState(0); 

    const [open, setOpen] = useState(false);

    const [selectedDate, setSelectedDate] = useState(props.selectedDate);

    const user = JSON.parse(localStorage.getItem('user'));

    /**
     * The state for the calendar
     */
    const [state, setState] = useState([
        {
          startDate: props.state[0].startDate,
          endDate:props.state[0].endDate,
          key: 'selection',
        //   label: selectedDate
        }
    ]);

    const handleSelection = (item) => {
        setPage(0);
        setPages(0);
        setState([item.selection]);
        setSelectedDate(item.selection.label || "Custom");
        setDrivers([]);
        // getData();
        setOpen(false);
    }

    /**
     * This is call is made in the parent component
     * because the drivers list is shared between
     * the table and the grid
     */
    const getData = (callback) => {
        const [selection] = state;
        const startDate = selection.startDate.toLocaleDateString("en-CA");
        const endDate = selection.endDate.toLocaleDateString("en-CA");
        const pageNumber = page + 1;
        setPage(pageNumber);
        const url = `https://uri-dev-cc-reliability-api-tbyptzvcgq-uk.a.run.app/v1/drivers?format=hours&city=${encodeURIComponent(city.city)}&limit=10&page=${pageNumber}&from=${startDate}&to=${endDate}`;
        axios.get(url,
        {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': user.token
            }
        }).then(response => {
                const {data} = response;
                setPages(data.pages);
                setDrivers([...drivers, ...data.items]);
                if(callback !== undefined) {
                    callback([...drivers, ...data.items]);
                }
                
                // return response.json();
        }).catch(err => console.log(err));
    }

    /**
     * This will update city data when dates are changed
     */
    const updateCityData = () => {
        const [selection] = state;
        const startDate = selection.startDate.toLocaleDateString("en-CA");
        const endDate = selection.endDate.toLocaleDateString("en-CA");
        const url = `https://uri-dev-cc-reliability-api-tbyptzvcgq-uk.a.run.app/v1/cities/${encodeURIComponent(city.city)}?format=hours&from=${startDate}&to=${endDate}`;
        axios.get(url,
        {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': user.token
            }
        }).then(response => {
            setCity(response.data);
            getData();
        })
    }
    
    useEffect(() => {
        getData();
    }, []);

    useEffect(() => {
        updateCityData();
    }, [state]);



    /**
     * For reseting pagination when parameters change
     */
    const resetPage = () => {
        setPage(-1);
    }

    return (
        <Main className=''>
            <div className="header">
                <h3>{city.city}</h3>
                <div className="optionButtons">
                    <ButtonGroup color="primary" aria-label="outlined primary button group">
                        <IconButton onClick={handleList}>
                            <List />
                        </IconButton>
                        <IconButton onClick={handleGrid}>
                            <Apps />
                        </IconButton>

                    </ButtonGroup>

                    <Button
                        variant="contained"
                        color="default"
                        startIcon={<CalendarToday />}
                        endIcon={<ArrowDropDownIcon />}
                        onClick={()=>setOpen(true)}
                    >
                        {selectedDate}
                    </Button>
                </div>

            </div>
            <br />
            <Grid container spacing={0} >
                <Grid item xs={12} sm={6} md={4} lg={2}>
                    <div className="noBorderItem">
                        <Typography variant="overline">Reliability</Typography>
                        <Typography variant="h4">{city.rate}%</Typography>
                    </div>
                </Grid>
                <Grid item xs={12} sm={6} md={4} lg={2}>
                    <div className="noBorderItem">
                        <Typography variant="overline">Trips</Typography>
                        <Typography variant="h4">{city.successTrips+city.cancelledTrips}</Typography>
                    </div>
                </Grid>
                <Grid item xs={12} sm={6} md={4} lg={2}>
                    <div className="noBorderItem">
                        <Typography variant="overline">Online HRS</Typography>
                        <Typography variant="h4">{city.online}</Typography>
                    </div>
                </Grid>
                <Grid item xs={12} sm={6} md={4} lg={2}>
                    <div className="noBorderItem">
                        <Typography variant="overline">Scheduled HRS</Typography>
                        <Typography variant="h4">{city.scheduled}</Typography>
                    </div>
                </Grid>
                <Grid item xs={12} sm={6} md={4} lg={2}>
                    <div className="noBorderItem">
                        <Typography variant="overline">Successful Rides</Typography>
                        <Typography variant="h4">{city.successTrips}</Typography>
                    </div>
                </Grid>
                <Grid item xs={12} sm={6} md={4} lg={2}>
                    <div className="noBorderItem">
                        <Typography variant="overline">Cancelled Rides</Typography>
                        <Typography variant="h4">{city.cancelledTrips}</Typography>
                    </div>
                </Grid>
            </Grid>
            <br />
            {
                grid && (
                    <Grid container spacing={1}>
                        {
                            drivers.map((element, index) =>
                                <Grid item xs={12} sm={6} lg={4}>
                                    <Card>
                                        <CardContent>
                                            <div className="cardHeader">
                                                <img src={element.image} alt="" className="topAvatar" />
                                                <div className="cardDriverName">
                                                    <p className="cardTitle">
                                                        {element.driver}
                                                    </p>
                                                    <Chip color="primary" clickable="true" size="small" label={element.status}> </Chip>
                                                </div>

                                            </div>

                                            <p className="cardAcceptanceRate">
                                                {element.rate}%
                                            </p>
                                            <div className="cardDetails">
                                                <span>Today:</span>
                                                <span className="featuredMoneyRate">
                                                    {console.log(element)}
                                                    {element.pastRate < element.rate && <><ArrowUpward className="featuredIcon" />{element.growth}%</>}
                                                    {element.pastRate > element.rate && <><ArrowDownward className="featuredIcon" style={{color: 'red'}} />{element.growth}%</>}
                                                    {element.pastRate === element.rate && <>{element.growth}%</>}
                                                </span>
                                            </div>
                                            <div className="cardDetails">
                                                <span>Yesterday:</span>
                                                <span className="featuredMoneyRate">{element.pastRate}%</span>
                                            </div>
                                            <Grid container spacing={1}>
                                                <Grid item xs={4}>
                                                    <div className="">
                                                        <span className="indicators">Trips</span>
                                                        <h4>{element.successTrips}</h4>
                                                    </div>
                                                </Grid>
                                                <Grid item xs={4}>
                                                    <div className="">
                                                        <span className="indicators">Cancelled</span>
                                                        <h4>{element.cancelledTrips}</h4>
                                                    </div>
                                                </Grid>
                                                <Grid item xs={4}>
                                                    <div className="">
                                                        <span className="indicators">Acceptance</span>
                                                        <h4>{element.pastRate} %</h4>
                                                    </div>
                                                </Grid>
                                                <Grid item xs={4}>
                                                    <div className="">
                                                        <span className="indicators">Scheduled</span>
                                                        <h4>{element.scheduled}</h4>
                                                    </div>
                                                </Grid>
                                                <Grid item xs={4}>
                                                    <div className="">
                                                        <span className="indicators">Online / Off</span>
                                                        <h4>{element.online} / <span className="indicators">{element.offline}</span></h4>
                                                    </div>
                                                </Grid>
                                                <Grid item xs={4}>
                                                    <div className="">
                                                        <span className="indicators">Total driven</span>
                                                        <h4>{element.total}</h4>
                                                    </div>
                                                </Grid>
                                            </Grid>
                                        </CardContent>
                                        <CardActions>
                                            <Button size="medium" color="primary">
                                                Download
                                            </Button>
                                            <Button size="medium" color="primary">
                                                Email
                                            </Button>
                                        </CardActions>
                                    </Card>
                                </Grid>
                            )
                        }
                    </Grid>
                )
            }
            {
                !grid && drivers.length > 0 &&
                (
                    <InfiniteTable drivers={drivers} resetPage={resetPage} pages={pages} pageNumber={page} getData={getData} />
                )
            }
            <Dialog open={open} onClose={()=>setOpen(false)}>
                <Calendar
                    handleSelection={handleSelection}
                    state={state}
                    selectedDate={selectedDate}
                />;
            </Dialog>
        </Main>
    )
}