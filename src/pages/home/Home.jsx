import React from 'react';
import Chart from '../../components/chart/Chart';
import FeaturedInfo from '../../components/featuredInfo/FeaturedInfo';

import GoogleMapReact from 'google-map-react';
import { WidgetSm } from '../../components/widgets/WidgetSm';
import { WidgetLg } from '../../components/widgets/WidgetLg';
import Main from '../../components/main/Main';
import { Grid, Paper } from '@material-ui/core';
import { ArrowDownward } from '@material-ui/icons';

const AnyReactComponent = ({ text }) => <div>{text}</div>;


export default function Home() {
    const defaultProps = {
        center: {
            lat: 48.382221,
            lng: -89.246109
        },
        zoom: 11
    };
    return (
        <Main className='overview'>
            <Grid className="actionGrid" container spacing={1}>
                <Grid item xs={1}>
                    <Paper>
                        <div className="featured">
                            <div className="featuredItem">
                                <span className="featuredTitle">Revenue</span>
                                <div className="featuredMoneyContainer">
                                    <span className="featuredMoney">$2300</span>
                                    <span className="featuredMoneyRate">-11.4<ArrowDownward className="featuredIcon negative" /></span>
                                </div>
                                <span className="featuredSub">
                                    Compared to last month
                                </span>
                            </div>
                        </div>
                    </Paper>
                </Grid>
            </Grid>
            <div className="home">
                {/* <FeaturedInfo /> */}

                <div style={{ height: '91vh' }}>

                    <GoogleMapReact
                        bootstrapURLKeys={{ key: "" }}
                        defaultCenter={defaultProps.center}
                        defaultZoom={defaultProps.zoom}
                    >
                        <AnyReactComponent
                            lat={48.385413}
                            lng={-89.337844}
                            text="My Marker"
                        />

                    </GoogleMapReact>
                </div>
                {/* <Chart />
            <div className="homeWidgets">
                <WidgetSm/>
                <WidgetLg/>
            </div> */}
            </div>

            
        </Main>

    );
}
