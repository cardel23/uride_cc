import { Box, Button, ButtonGroup, Card, CardActions, CardContent, Chip, Grid, IconButton, makeStyles, Select, Tab, Tabs, Typography, Input, MenuItem, InputLabel, FormControl, createMuiTheme, ThemeProvider, withStyles, createTheme } from '@material-ui/core';
import React, { useEffect } from 'react'
import PropTypes from 'prop-types';
import { Add, Apps, ArrowDropDown, ArrowUpward, CalendarToday, List, Pause } from '@material-ui/icons';
import TabPanel, { a11yProps } from '../../components/tabs/TabPanel';
import Main from '../../components/main/Main';
import { useState } from 'react';
import { green } from '@material-ui/core/colors';
import NotificationRules from '../../components/notifications/NotificationRules';
import { NotificationMessages } from '../../components/notifications/NotificationMessages';

const useTabStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        // minWidth: 120,
        // maxWidth: 300,
        width: "100%"
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: 2,
    },
    noLabel: {
        marginTop: theme.spacing(3),
    },
    root: {
        justifyContent: "center",
        flexGrow: 1,
    },
    paper: {
        textAlign: 'center',
        border: 'none'
    },
    scroller: {
        flexGrow: "0"
    },
    tabContainer: {
        width: '100%',
        flexGrow: "1"
    },
    button: {
        marginLeft: '4px',
        marginRight: '4px'
    },
    dropdown: {
        margin: '8px',
        paddingTop: "8px",
        width: "100%",
    },
    cardActions: {
        display: "flex",
        padding: "8px",
        alignItems: "center",
        justifyContent: "space-between"
    }
}));

const GreenButton = withStyles((theme) => ({
    root: {
      color: theme.palette.getContrastText(green[700]),
      backgroundColor: green[500],
      '&:hover': {
        backgroundColor: green[700],
      },
    },
}))(Button);

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

// const defaultTheme = createTheme();
// const theme = createTheme({
//     palette: {
//         tertiary: defaultTheme.palette.augmentColor({
//             color: { main: green[500] },
//             name: "tertiary"
//         })
//     }
//   }
// );
export const Notifications = (props) => {
    const classes = useTabStyles();
    const msgs = [
        {
            id: "0",
            title: "Shift about to start",
            text: "Heads up! Your scheduled shift it’s about to begin please remember to set your status to online.",
            edit: false
        },
        {
            id: "1",
            title: "Unscheduled driver",
            text: "Hey seems you haven't send your schedule to the operation team, you might want to do that before going online.",
            edit: false
        },
        {
            id: "2",
            title: "Offline driver",
            text: "Hey! We have scheduled to drive right now but for reason you are not online. Please contact dispatch!",
            edit: false
        },
    ];
    
    const ruleArray = [
        {
            id: "0",
            title: "Shift starts in 30 min",
            status: "Active",
            scope: [],
            msg: "0",
            description: "30 minutes reminder before starting a shift",
            running: true,
            edit: false
        },
        {
            id: "1",
            title: "Shift starts in 15 min",
            status: "Active",
            scope: [],
            msg: "0",
            description: "15 minutes reminder before starting a shift",
            running: false,
            edit: false
        },
        {
            id: "2",
            title: "Unscheduled",
            status: "Pending activation",
            scope: ["North Bay", "Timmins", "In-app", "SMS", "Email"],
            msg: "1",
            description: "Reminder to unscheduled drivers to contact dispatch if they want to go online.",
            running: true,
            edit: false
        },
        {
            id: "3",
            title: "Offline after 15 min",
            status: "Active",
            scope: [],
            msg: "2",
            description: "Offline for 15 minutes on active shift.",
            running: false,
            edit: false
        },
    ];

    const scopeArray = [
        "Full-time",
        "Part-time",
        "In-app",
        "SMS",
        "Email",
        "Scheduled & offline",
        "All cities",
        "Belleville",
        "Chatham-Kent",
        "North Bay",
        "Peterborough",
        "Sault Ste Marie",
        "Sudbury",
        "Thunder Bay",
        "Timmins"
    ]

    const [value, setValue] = React.useState(0);
    const [messages, setMessages] = useState(msgs);
    const [rules, setRules] = useState(ruleArray);
    const [scopes, setScopes] = useState(scopeArray);

    useEffect(() => {
        props.setAppbarTitle(props.title);
    }, [])

    useEffect(() => {
        // console.log(rules);
    },[rules]);

    useEffect(() => {
        // console.log(messages);
    }, [messages]);


    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleSelect = (event, index) => {
        let tempRules = [...rules];
        tempRules[index].msg = event.target.value;
        setRules(tempRules);
    };

    const handleScopeChange = (event, index) => {
        let tempRules = [...rules];
        tempRules[index].scope= event.target.value;
        setRules(tempRules);
    };

    const handleTitleChange = (event, index) => {
        let msgs = [...messages];
        const msg = msgs[index];
        msg.title = event.target.value;
    }

    const handleTextChange = (event, index) => {
        debugger;
        let msgs = [...messages];
        const msg = msgs[index];
        msg.text = event.target.value;
        setMessages(msgs);
    }

    const onUpdateRules = (rules) => {
        setRules(rules);
    }

    const onUpdateMessages = (messages) => {
        setMessages(messages);
    }

    return (
        <Main className='mainContent'>
            <Tabs
                value={value}
                onChange={handleChange}
                indicatorColor="primary"
                textColor="primary"
                variant="scrollable"
                scrollButtons="auto"
                aria-label="scrollable auto tabs example">

                <Tab label="Rules" {...a11yProps(0)} />
                <Tab label="Messages" {...a11yProps(1)} />
                <Tab label="Logs" {...a11yProps(2)} />

            </Tabs>
            <br />
            <TabPanel value={value} index={0}>
                <NotificationRules
                rules={rules}
                scopes={scopes}
                handleSelect={handleSelect}
                handleScopeChange={handleScopeChange}
                handleTitleChange={handleTitleChange}
                msgs={messages}
                classes={classes}
                onUpdateRules={onUpdateRules}
                />
            </TabPanel>
            <TabPanel value={value} index={1}>
                <NotificationMessages
                rules={rules}
                scopes={scopes}
                msgs={messages}
                classes={classes}
                handleTextChange={handleTextChange}
                onUpdateMessages={onUpdateMessages}
                />
            </TabPanel>
        </Main>
    )
}

