import { Button, ButtonGroup, Card, CardActions, CardContent, Chip, Grid, IconButton, makeStyles, Tab, Tabs } from '@material-ui/core';
import React, { useEffect, useState } from 'react'
import { Apps, ArrowDropDown, ArrowUpward, CalendarToday, List } from '@material-ui/icons';
import TabPanel, { a11yProps } from '../../components/tabs/TabPanel';
import DriverTable from '../reliability/DriverTable';
import Main from '../../components/main/Main';

const useTabStyles = makeStyles({
    root: {
        justifyContent: "center",
        flexGrow: 1,
    },
    paper: {
        textAlign: 'center',
        border: 'none'
    },
    scroller: {
        flexGrow: "0"
    },
    tabContainer: {
        width: '100%',
        flexGrow: "1"
    }
});

export const Drivers = (props) => {
    const classes = useTabStyles();
    const [value, setValue] = React.useState(0);
    const [grid, setGrid] = useState(false);

    const handleGrid = () => setGrid(true);
    const handleList = () => setGrid(false);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const [drivers, setDrivers] = useState([]);
    const getData = () => {
        fetch('assets/users.json'
            , {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
            }
        )
            .then(function (response) {
                console.log(response)
                return response.json();
            })
            .then(function (myJson) {
                setDrivers(myJson);
            });
    }
    useEffect(() => {
        props.setAppbarTitle(props.title);
        getData();
    }, []);


    return (
        <Main className="mainContent">
            <Tabs
                value={value}
                onChange={handleChange}
                indicatorColor="primary"
                textColor="primary"
                variant="scrollable"
                scrollButtons="auto"
                aria-label="scrollable auto tabs example">

                <Tab label="Roster" {...a11yProps(0)} />
                <Tab label="Suspended" {...a11yProps(1)} />
                <Tab label="Disabled" {...a11yProps(2)} />
                <Tab label="Prospects" {...a11yProps(3)} />

            </Tabs>

            <TabPanel value={value} index={0}>
                <div className="header">
                    <h3>Reliability Summary</h3>
                    <div className="optionButtons">
                        <ButtonGroup color="primary" aria-label="outlined primary button group">
                            <IconButton onClick={handleList}>
                                <List />
                            </IconButton>
                            <IconButton onClick={handleGrid}>
                                <Apps />
                            </IconButton>

                        </ButtonGroup>

                        <Button
                            variant="contained"
                            color="default"
                            className={classes.button}
                            startIcon={<CalendarToday />}
                            endIcon={<ArrowDropDown />}
                        >
                            Today
                        </Button>
                    </div>

                </div>
                <br />
                {
                    grid && (
                        <Grid container spacing={1}>
                            {
                                drivers.map((element, index) =>
                                    <Grid item xs={12} sm={6} lg={3}>
                                        <Card>
                                            <CardContent>
                                                <div className="cardHeader">
                                                    <img src="https://images.pexels.com/photos/8050897/pexels-photo-8050897.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500" alt="" className="topAvatar" />
                                                    <div className="cardDriverName">
                                                        <p className="cardTitle">
                                                            {element.name}
                                                        </p>
                                                        <Chip color="primary" clickable="true" size="small" label={element.status}> </Chip>
                                                    </div>

                                                </div>

                                                <p className="cardAcceptanceRate">
                                                    {element.reliability}%
                                                </p>
                                                <div className="cardDetails">
                                                    <span>Today:</span>
                                                    <span className="featuredMoneyRate"><ArrowUpward className="featuredIcon" />{element.growth}%</span>
                                                </div>
                                                <div className="cardDetails">
                                                    <span>Yesterday:</span>
                                                    <span className="featuredMoneyRate">{element.lastAcceptance}%</span>
                                                </div>
                                                <div className="cardDetails pd-v16">

                                                    <div className="">
                                                        <span className="indicators">Trips</span>
                                                        <h3>{element.trips}</h3>
                                                    </div>
                                                    <div className="">
                                                        <span className="indicators">Schedule</span>
                                                        <h3>{element.scheduled}</h3>
                                                    </div>
                                                    <div className="">
                                                        <span className="indicators">Online</span>
                                                        <h3>{element.online}</h3>
                                                    </div>
                                                    <div className="">
                                                        <span className="indicators">Offline</span>
                                                        <h3>{element.offline}</h3>
                                                    </div>
                                                </div>
                                            </CardContent>
                                            <CardActions>
                                                <Button size="medium" color="primary">
                                                    Download
                                                </Button>
                                                <Button size="medium" color="primary">
                                                    Email
                                                </Button>
                                            </CardActions>
                                        </Card>
                                    </Grid>
                                )
                            }
                        </Grid>
                    )
                }
                {
                    !grid &&
                    (
                        <DriverTable drivers={drivers} />
                    )
                }
            </TabPanel>

        </Main>
    )
}