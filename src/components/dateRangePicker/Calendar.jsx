import { addDays, addMonths, endOfDay, endOfMonth, endOfWeek, startOfDay, startOfMonth, startOfWeek } from 'date-fns';
import React from 'react';
import { DateRangePicker } from 'react-date-range';
import 'react-date-range/dist/styles.css'; // main css file
import 'react-date-range/dist/theme/default.css'; // theme css file

/**
 * This is the component used for building the DatePicker selector for the reliability reports
 * @param {*} param0 
 * @returns 
 */
export const Calendar = ({state, selectedDate, handleSelection}) => {
    
    const customDateRanges = [
        {
            label: 'Today',
            hasCustomRendering: false,
            range: () => ({
                startDate: new Date(),
                endDate: new Date(),
                label: 'Today' 
            }),
            isSelected() {
                return selectedDate === this.label;
            }
        },
        {
            label: 'Yesterday',
            hasCustomRendering: false,
            range: () => ({
                startDate: addDays(new Date(),-1),
                endDate: addDays(new Date(),-1),
                label: 'Yesterday'
            }),
            isSelected() {
                return selectedDate === this.label;
            }
        },
        {
            label: 'This week',
            hasCustomRendering: false,
            range: () => ({
                startDate: defineds.startOfWeek,
                endDate: defineds.endOfWeek,
                label: 'This week'
            }),
            isSelected() {
                return selectedDate === this.label;
            }
        },
        {
            label: 'Last week',
            hasCustomRendering: false,
            range: () => ({
                startDate: defineds.startOfLastWeek,
                endDate: defineds.endOfLastWeek,
                label: 'Last week'
            }),
            isSelected() {
                return selectedDate === this.label;
            }
        },
        {
            label: 'Last 7 days',
            hasCustomRendering: false,
            range: () => ({
                startDate: defineds.endOfLastWeek,
                endDate: defineds.endOfToday,
                label: 'Last 7 days'
            }),
            isSelected() {
                return selectedDate === this.label;
            }
        },
        {
            label: 'This month',
            hasCustomRendering: false,
            range: () => ({
                startDate: defineds.startOfMonth,
                endDate: defineds.endOfMonth,
                label: 'This month'
            }),
            isSelected() {
                return selectedDate === this.label;
            }
        },
        {
            label: 'Last month',
            hasCustomRendering: false,
            range: () => ({
                startDate: defineds.startOfLastMonth,
                endDate: defineds.endOfLastMonth,
                label: 'Last month'
            }),
            isSelected() {
                return selectedDate === this.label;
            }
        },
        {
            label: 'Last 30 days',
            hasCustomRendering: false,
            range: () => ({
                startDate: startOfDay(addDays(new Date(), -30)),
                endDate: defineds.endOfToday,
                label: 'Last 30 days'
            }),
            isSelected() {
                return selectedDate === this.label;
            }
        },
        {
            label: 'Last 90 days',
            hasCustomRendering: false,
            range: () => ({
                startDate: startOfDay(addDays(new Date(), -90)),
                endDate: defineds.endOfToday,
                label: 'Last 90 days'
            }),
            isSelected() {
                return selectedDate === this.label;
            }
        },
    ];

    return (
        <DateRangePicker
            onChange={item => handleSelection(item)}
            showSelectionPreview={false}
            moveRangeOnFirstSelection={false}
            months={2}
            ranges={state}
            staticRanges={customDateRanges}
            scroll={{ enabled: true }}
            direction="vertical"
        />
    )
}

const defineds = {
    startOfWeek: startOfWeek(new Date()),
    endOfWeek: endOfWeek(new Date()),
    startOfLastWeek: startOfWeek(addDays(new Date(), -7)),
    endOfLastWeek: endOfWeek(addDays(new Date(), -7)),
    startOfToday: startOfDay(new Date()),
    endOfToday: endOfDay(new Date()),
    startOfYesterday: startOfDay(addDays(new Date(), -1)),
    endOfYesterday: endOfDay(addDays(new Date(), -1)),
    startOfMonth: startOfMonth(new Date()),
    endOfMonth: endOfMonth(new Date()),
    startOfLastMonth: startOfMonth(addMonths(new Date(), -1)),
    endOfLastMonth: endOfMonth(addMonths(new Date(), -1)),
};

