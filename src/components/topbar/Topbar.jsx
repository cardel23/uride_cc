import React from 'react';

import { NotificationsNone, Settings, Language} from "@material-ui/icons";

export default function Topbar() {
    return ( 
        <div className='topbar'>
            <div className="topbarWrapper">
                <div className="topLeft">
                    {/* <span className="logo">
                        Uride
                    </span> */}
                </div>
                <div className="topRight">
                    <div className="topbarIconContainer">
                        {/* <NotificationsNone />
                        <span className="topIconBadge">2</span> */}
                    </div>
                    <div className="topbarIconContainer">
                        {/* <Language /> */}
                        {/* <span className="topIconBadge">2</span> */}
                    </div>
                    <div className="topbarIconContainer">
                        {/* <Settings /> */}
                        {/* <span className="topIconBadge">2</span> */}
                    </div>
                    {/* <img src="https://images.pexels.com/photos/8050897/pexels-photo-8050897.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500" alt="" className="topAvatar" /> */}
                </div>
            </div>
        </div>
     );
}