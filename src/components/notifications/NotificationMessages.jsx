import React, { useEffect, useState } from 'react'
import { Box, Button, ButtonGroup, Card, CardActions, CardContent, Chip, Grid, IconButton, makeStyles, Select, Tab, Tabs, Typography, Input, MenuItem, InputLabel, FormControl, ThemeProvider, TextField, withStyles, createTheme } from '@material-ui/core';
import { green } from '@material-ui/core/colors';
import { Add, Pause } from '@material-ui/icons';


const GreenButton = withStyles((theme) => ({
    root: {
        color: theme.palette.getContrastText(green[700]),
        backgroundColor: green[500],
        '&:hover': {
            backgroundColor: green[700],
        },
    },
}))(Button);

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: '-webkit-fill-available',
        },
    },
}));

export const NotificationMessages = (props) => {

    const [messages, setMessages] = useState(props.msgs);
    const [oldMessages, setOldMessages] = useState([]);

    var oldMsgs = [];
    const classes = useStyles();

    const [editing, setEditing] = useState(false);

    const onEdit = (message) => {

    };

    useEffect(() => {
        updateOldMessages();
    }, []);

    const updateOldMessages = () => {
        oldMsgs = [];
        messages.map((msg) => {
            oldMsgs.push({ ...msg })
        });
        setOldMessages(oldMsgs);
    }

    const handleTitleChange = (event, index) => {
        let msgs = [...messages];
        const msg = msgs[index];
        msg.title = event.target.value;
        setMessages(msgs);
    };

    const handleTextChange = (event, index) => {
        let msgs = [...messages];
        const msg = msgs[index];
        msg.text = event.target.value;
        setMessages(msgs);
    };

    const enableEdit = (index) => {
        debugger;
        setEditing(true);
        let msgs = [...messages];
        let msg = msgs[index];
        msg.edit = true;
        setMessages(messages);
    };

    const disableEdit = () => {
        let msgs = [...messages];
        msgs.map((msg) => msg.edit = false);
        setEditing(false);
        setMessages(oldMessages);
    };

    const createMessage = () => {
        setEditing(true);
        const newMessage = {
            id: "",
            title: "",
            text: "",
            edit: true
        }
        let msgs = [...messages];
        msgs.push(newMessage);
        setMessages(msgs);
    };

    const saveMessage = (msg, index) => {
        debugger;
        let msgs = [...messages];
        if (msg.id === "")
            msg.id = "" + (Math.random() * 10000).toFixed(0);
        msg.edit = false;
        msgs[index] = msg;
        setMessages(msgs);
        updateOldMessages();
        setEditing(false);
        props.onUpdateMessages(msgs);
    };

    const cancelMessage = (msg, index) => {
        debugger;
        let msgs = [...messages];
        if (msg.id === "") {
            msgs = msgs.filter(msg => msg.id !== "");
            setMessages(oldMessages);
            setEditing(false);
        } else {
            disableEdit();
        }
    };

    // console.log(messages);
    return (
        <div>
            <div className="header">
                <h3>Notification messages</h3>
                <div className="optionButtons">

                    <Button
                        onClick={createMessage}
                        disabled={editing}
                        variant="contained"
                        color="primary"
                        className={props.classes.button}
                        startIcon={<Add />}
                    >
                        Create new
                    </Button>
                </div>

            </div>
            <br />
            <Grid container spacing={1}>
                {
                    messages.map((msg, index) => (
                        <Grid key={"message-" + index} className="" item xs={12} sm={6} lg={3}>
                            <Card className="spaceBetween flexColumn">
                                <CardContent>
                                    {
                                        msg.edit &&
                                        <FormControl disabled={!msg.edit}>
                                            <InputLabel htmlFor="title">Title</InputLabel>
                                            <Input id="title" value={msg.title} onChange={(event) => handleTitleChange(event, index)} />
                                        </FormControl>
                                    }
                                    {
                                        !msg.edit &&
                                        <p className="cardTitle">
                                            {msg.title}
                                        </p>
                                    }
                                    <form className={classes.root} noValidate autoComplete="off">
                                        <br />
                                        <FormControl disabled={!msg.edit}>
                                            <InputLabel htmlFor="text">Text</InputLabel>
                                            <Input multiline minRows={5} maxRows={5} id="text" value={msg.text} onChange={(event) => handleTextChange(event, index)} />
                                        </FormControl>
                                    </form>
                                </CardContent>
                                <CardActions className={props.classes.cardActions}>
                                    {
                                        !msg.edit &&
                                        <>
                                            <Button size="medium" onClick={() => enableEdit(index)} color="primary">
                                                Edit
                                            </Button>
                                            <Button size="medium" color="secondary">
                                                Delete
                                            </Button>
                                        </>
                                    }
                                    {
                                        msg.edit &&
                                        <>
                                            <Button size="medium" onClick={() => saveMessage(msg, index)} color="primary">
                                                Save
                                            </Button>
                                            <Button size="medium" onClick={() => cancelMessage(msg, index)} color="secondary">
                                                Cancel
                                            </Button>
                                        </>
                                    }
                                </CardActions>
                            </Card>
                        </Grid>
                    ))
                }
            </Grid>
        </div>
    )
}
