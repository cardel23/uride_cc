import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid'; 
import Chart from '../../components/chart/Chart';
import { Chip, Typography, Divider} from '@material-ui/core';
import { ArrowDownward, ArrowUpward } from "@material-ui/icons";
import Main from '../main/Main';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    border: 'none'
  },
}));

export default function CenteredGrid() {
  const classes = useStyles();

  return (
    <Main className="mainContent">

    <div className={classes.root}>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <Paper className="dashboardItem">
            <Typography variant='h6' >Key KPI</Typography>
            <Grid container spacing={0} >
              <Grid item xs={12} sm={6} md={4} lg={2}>
                <div className="noBorderItem">
                  <Typography variant="overline">Max dispatch time</Typography>
                  <Typography variant="h4">72</Typography>
                </div>
              </Grid>
              <Grid item xs={12} sm={6} md={4} lg={2}>
                <div className="noBorderItem">
                  <Typography variant="overline">Avg dispatch</Typography>
                  <Typography variant="h4">72</Typography>
                </div>
              </Grid>
              <Grid item xs={12} sm={6} md={4} lg={2}>
                <div className="noBorderItem">
                  <Typography variant="overline">Avg Km/Ride</Typography>
                  <Typography variant="h4">72</Typography>
                </div>
              </Grid>
              <Grid item xs={12} sm={6} md={4} lg={2}>
                <div className="noBorderItem">
                  <Typography variant="overline">Avg driver earning</Typography>
                  <Typography variant="h4">72</Typography>
                </div>
              </Grid>
              <Grid item xs={12} sm={6} md={4} lg={2}>
                <div className="noBorderItem">
                  <Typography variant="overline">Avg onboard</Typography>
                  <Typography variant="h4">72</Typography>
                </div>
              </Grid>
              <Grid item xs={12} sm={6} md={4} lg={2}>
                <div className="noBorderItem">
                  <Typography variant="overline">Avg collection</Typography>
                  <Typography variant="h4">72</Typography>
                </div>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={2} xl={2}>
          <Paper>
            <div className="noBorderItem">
              <h3 className="indicatorTitle">Ride requests</h3>
              <Typography variant="h4">1143</Typography>
              <span className="featuredMoneyRate">-11.4%<ArrowDownward className="featuredIcon negative"/></span>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={2} xl={2}>
          <Paper>
            <div className="noBorderItem">
              <h3 className="indicatorTitle">Ride requests</h3>
              <Typography variant="h4">1143</Typography>
              <span className="featuredMoneyRate">-11.4%<ArrowDownward className="featuredIcon negative"/></span>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={2} xl={2}>
          <Paper>
            <div className="noBorderItem">
              <h3 className="indicatorTitle">Ride requests</h3>
              <Typography variant="h4">1143</Typography>
              <span className="featuredMoneyRate">-11.4%<ArrowDownward className="featuredIcon negative"/></span>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={2} xl={2}>
          <Paper>
            <div className="noBorderItem">
              <h3 className="indicatorTitle">Ride requests</h3>
              <Typography variant="h4">1143</Typography>
              <span className="featuredMoneyRate">-11.4%<ArrowDownward className="featuredIcon negative"/></span>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={2} xl={2}>
          <Paper>
            <div className="noBorderItem">
              <h3 className="indicatorTitle">Ride requests</h3>
              <Typography variant="h4">1143</Typography>
              <span className="featuredMoneyRate">-11.4%<ArrowUpward className="featuredIcon"/></span>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={2} xl={2}>
          <Paper>
            <div className="noBorderItem">
              <h3 className="indicatorTitle">Ride requests</h3>
              <Typography variant="h4">1143</Typography>
              <span className="featuredMoneyRate">-11.4%<ArrowDownward className="featuredIcon negative"/></span>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={4}>
          <Paper>
            <div className="noBorderItem">
              <div className="featuredTitle">
                <h3 className="indicatorTitle">Collection</h3>
                <Chip color="primary" clickable="true" size="small" label="Hourly"> </Chip>
              </div>
              <Divider />
              <Chart />
            </div>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={4}>
          <Paper>
            <div className="noBorderItem">
              <div className="featuredTitle">
                <h3 className="indicatorTitle">Sucess rate (%)</h3>
                <Chip color="primary" clickable="true" size="small" label="Hourly"> </Chip>
              </div>
              <Divider />
              <Chart />
            </div>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={4}>
          <Paper>
            <div className="noBorderItem">
              <div className="featuredTitle">
                <h3 className="indicatorTitle">Rides by class</h3>
                <Chip color="primary" clickable="true" size="small" label="Hourly"> </Chip>
              </div>
              <Divider />
              <Chart />
            </div>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={2} xl={2}>
          <Paper>
            <div className="noBorderItem">
              <h3 className="indicatorTitle">Ride requests</h3>
              <Typography variant="h4">1143</Typography>
              <span className="featuredMoneyRate">-11.4%<ArrowDownward className="featuredIcon negative"/></span>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={2} xl={2}>
          <Paper>
            <div className="noBorderItem">
              <h3 className="indicatorTitle">Ride requests</h3>
              <Typography variant="h4">1143</Typography>
              <span className="featuredMoneyRate">-11.4%<ArrowDownward className="featuredIcon negative"/></span>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={2} xl={2}>
          <Paper>
            <div className="noBorderItem">
              <h3 className="indicatorTitle">Ride requests</h3>
              <Typography variant="h4">1143</Typography>
              <span className="featuredMoneyRate">-11.4%<ArrowDownward className="featuredIcon negative"/></span>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={2} xl={2}>
          <Paper>
            <div className="noBorderItem">
              <h3 className="indicatorTitle">Ride requests</h3>
              <Typography variant="h4">1143</Typography>
              <span className="featuredMoneyRate">-11.4%<ArrowDownward className="featuredIcon negative"/></span>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={2} xl={2}>
          <Paper>
            <div className="noBorderItem">
              <h3 className="indicatorTitle">Ride requests</h3>
              <Typography variant="h4">1143</Typography>
              <span className="featuredMoneyRate">-11.4%<ArrowUpward className="featuredIcon"/></span>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={2} xl={2}>
          <Paper>
            <div className="noBorderItem">
              <h3 className="indicatorTitle">Ride requests</h3>
              <Typography variant="h4">1143</Typography>
              <span className="featuredMoneyRate">-11.4%<ArrowDownward className="featuredIcon negative"/></span>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={6}>
          <Paper className={classes.paper}>xs=6</Paper>
        </Grid>
        <Grid item xs={6}>
          <Paper className={classes.paper}>xs=6</Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className={classes.paper}>xs=3</Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className={classes.paper}>xs=3</Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className={classes.paper}>xs=3</Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className={classes.paper}>xs=3</Paper>
        </Grid>
      </Grid>
    </div>
    </Main>

  );
}