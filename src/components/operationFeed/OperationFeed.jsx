import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { Filter1 } from '@material-ui/icons';
import { Chip, Divider } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        // maxWidth: 345,
        height: '100%',
        '&:hover': {

        }
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
}));

const feed = [
    {
        name: 'Cody Fisher',
        status: 'En Route',
        action: 'Accepted a trip from Darlene Robertson',
    },
    {
        name: 'Eleanor Peña',
        status: 'On queue',
        action: 'Is requesting a ride',
    },
    {
        name: 'Cody Fisher',
        status: 'Dropped off',
        action: 'Arrived at Brooklyn Simmons drop off location',
    },
]

export default function OperationFeed({ title, icon, eventFunction }) {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    const ActionIcon = icon;


    return (
        <Card className={classes.root}>
            <CardHeader
                action={
                    <IconButton onClick={eventFunction} aria-label="settings">
                        {
                            ActionIcon &&
                            <ActionIcon />
                        }
                        {
                            !ActionIcon &&
                            <Filter1 />
                        }
                        {/* <ActionIcon/> */}
                    </IconButton>
                }
                title={title ? title : "Operation Feed"}
            />
            <Divider />
            <CardContent>
                {
                    feed.map((element, index) =>
                        <div id={'item' + index} className="card" >
                            <div className="cardHeader">
                                <img src="https://images.pexels.com/photos/8050897/pexels-photo-8050897.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500" alt="" className="topAvatar" />
                                <div className="cardDriverName">
                                    <p className="cardTitle">
                                        {element.name}
                                    </p>
                                    <Chip color="primary" clickable="true" size="small" label={element.status}> </Chip>
                                </div>
                            </div>
                            <Typography variant="caption">{element.action}</Typography>

                        </div>
                    )
                }
            </CardContent>

        </Card>
    );
}
