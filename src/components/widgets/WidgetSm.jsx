import React from 'react'
import "./widgetSm.css";
import { Visibility } from "@material-ui/icons";

export const WidgetSm = () => {
    return (
        <div className="widgetSm">
            <span className="widgetSmTitle">New Members</span>
            <ul className="widgetSmList">
                <li className="widgetSmListItem">
                    <img src="" alt="" />
                    <div className="widgetSmUser">
                        <span className="widgetSmUsername">Coca Leca</span>
                        <span className="widgetSmUserTitle">Web developer</span>
                    </div>
                    <button className="widgetSmButton">
                        <Visibility className="widgetSmIcon"/>
                        Display
                    </button>
                </li>
                <li className="widgetSmListItem">
                    <img src="" alt="" />
                    <div className="widgetSmUser">
                        <span className="widgetSmUsername">Coca Leca</span>
                        <span className="widgetSmUserTitle">Web developer</span>
                    </div>
                    <button className="widgetSmButton">
                        <Visibility className="widgetSmIcon"/>
                        Display
                    </button>
                </li>
                <li className="widgetSmListItem">
                    <img src="" alt="" />
                    <div className="widgetSmUser">
                        <span className="widgetSmUsername">Coca Leca</span>
                        <span className="widgetSmUserTitle">Web developer</span>
                    </div>
                    <button className="widgetSmButton">
                        <Visibility className="widgetSmIcon"/>
                        Display
                    </button>
                </li>
                <li className="widgetSmListItem">
                    <img src="" alt="" />
                    <div className="widgetSmUser">
                        <span className="widgetSmUsername">Coca Leca</span>
                        <span className="widgetSmUserTitle">Web developer</span>
                    </div>
                    <button className="widgetSmButton">
                        <Visibility className="widgetSmIcon"/>
                        Display
                    </button>
                </li>
            </ul>
        </div>
    )
}
