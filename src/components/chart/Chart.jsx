import React, { } from 'react';
import { LineChart, Line, XAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';

import "./chart.css";

export default function Chart() {

  const data = [
    {
      name: 'Jan',
      "Active Users": 1240,
      pv: 2400,
      amt: 2400,
    },
    {
      name: 'Feb',
      "Active Users": 1109,
      pv: 2400,
      amt: 2400,
    },
    {
      name: 'Mar',
      "Active Users": 968,
      pv: 2400,
      amt: 2400,
    },
    {
      name: 'Apr',
      "Active Users": 1459,
      pv: 2400,
      amt: 2400,
    },
    {
      name: 'May',
      "Active Users": 2324,
      pv: 2400,
      amt: 2400,
    },
    {
      name: 'Jun',
      "Active Users": 4000,
      pv: 2400,
      amt: 2400,
    },
    {
      name: 'Jul',
      "Active Users": 3000,
      pv: 1398,
      amt: 2210,
    },
    {
      name: 'Aug',
      "Active Users": 2000,
      pv: 9800,
      amt: 2290,
    },
    {
      name: 'Sept',
      "Active Users": 2780,
      pv: 3908,
      amt: 2000,
    },
    {
      name: 'Oct',
      "Active Users": 0,
      pv: 4800,
      amt: 2181,
    },
    {
      name: 'Nov',
      "Active Users": 0,
      pv: 3800,
      amt: 2500,
    },
    {
      name: 'Dec',
      "Active Users": 0,
      pv: 4300,
      amt: 2100,
    },
  ];

  return (
    <div /*className="chart"*/>
      {/* <h3 className="chartTitle">
                User Analytics
            </h3> */}

      <ResponsiveContainer width="100%" aspect={4 / 1}>
        <LineChart data={data}>
          <XAxis dataKey="name" stroke="#5550bd" />
          <Line type="monotone" dataKey="Active Users" stroke="#5550bd" />
          <Tooltip />
          <CartesianGrid stroke="#e0dfdf" strokeDasharray="6 6" />
        </LineChart>
      </ResponsiveContainer>
    </div>
  );
}