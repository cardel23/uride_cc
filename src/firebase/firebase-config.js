import firebase from "firebase/compat/app";
import 'firebase/compat/firestore';
import 'firebase/compat/auth';

// const firebaseConfig = {
//     apiKey: process.env.REACT_APP_APIKEY,
//     authDomain: process.env.REACT_APP_AUTHDOMAIN,
//     databaseURL: process.env.REACT_APP_DATABASEURL,
//     projectId: process.env.REACT_APP_PROJECTID,
//     storageBucket: process.env.REACT_APP_STORAGEBUCKET,
//     messagingSenderId: process.env.REACT_APP_MESSAGINGSENDERID,
//     appId: process.env.REACT_APP_APPID,
//     measurementId: process.env.REACT_APP_MEASUREMENTID
// };

const firebaseConfig = {
  apiKey: "AIzaSyDsjaS9diUQM8wtvoqQ1mu2pu8u09ny7wI",
  authDomain: "sample-654d9.firebaseapp.com",
  databaseURL: "https://sample-654d9.firebaseio.com",
  projectId: "sample-654d9",
  storageBucket: "sample-654d9.appspot.com",
  messagingSenderId: "248227702591",
  appId: "1:248227702591:web:ee5aff96e4ff1d1587958a"
};

// if (process.env.NODE_ENV === 'test') {
//   // Initialize Firebase
//   firebase.initializeApp(firebaseConfigTest);
// } else {
//   // Initialize Firebase
//   firebase.initializeApp(firebaseConfig);
// }

// console.log(process.env);

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
const fb = firebase;
export {
    db, googleAuthProvider, fb
};