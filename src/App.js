import Topbar from "./components/topbar/Topbar";
import "./App.css";
import MiniDrawer from "./components/miniDrawer/MiniDrawer";
import { AppRouter } from "./routers/AppRouter";
import { Provider } from 'react-redux'
import { store } from './store/store'

function App() {
  return (
    <div className="App">
      <Topbar />
      <div className="container">
        <Provider store={store}>
          <AppRouter />
        </Provider>
      </div>
    </div>
  );
}

export default App;
