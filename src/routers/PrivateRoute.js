import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import PropTypes from 'prop-types'

/**
 * Para proteger las rutas
 * @param {*} param0 
 * @returns 
 */
export const PrivateRoute = ({
    isAuth,
    component: Component,
    // resto de los argumentos
    ...rest
}) => {

    // grabar la ruta cuando cambia
    // if(isAuth)
    //     localStorage.setItem('lastPath', rest.location.pathname);

    return (
        
        <Route
            {...rest}
            component={
                // callback para llamar al component similar a 
                // useState
                (props) => (
                    (isAuth)
                        ? (<>{localStorage.setItem('lastPath', rest.location.pathname)}<Component {...props}/></>)
                        : (<Redirect to='/auth/login'/>)
                )
            }
        />
    )
}

PrivateRoute.propTypes = {
    isAuth: PropTypes.bool.isRequired,
    component: PropTypes.func.isRequired

}
