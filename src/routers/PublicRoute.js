import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import PropTypes from 'prop-types';

export const PublicRoute = (
{
    isAuth,
    component: Component,
    // resto de los argumentos
    ...rest
}) => {
    return (
        
        <Route
            {...rest}
            component={
                // callback para llamar al component similar a 
                // useState
                (props) => (
                    (!isAuth)
                        ?
                         <Component {...props}/>
                        : (<Redirect to='/'/>)
                )
            }
        />
    )
}

PublicRoute.propTypes = {
    isAuth: PropTypes.bool.isRequired,
    component: PropTypes.func.isRequired

}
