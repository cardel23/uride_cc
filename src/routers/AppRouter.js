import React, { useEffect, useState } from 'react'
import { 
    BrowserRouter as Router, 
    Redirect, 
    Switch 
} from 'react-router-dom';
import { AuthRouter } from './AuthRouter'
import { fb } from "../firebase/firebase-config";
import { useDispatch, useSelector } from 'react-redux';
import { PrivateRoute } from './PrivateRoute';
import { PublicRoute } from './PublicRoute';
import MiniDrawer from '../components/miniDrawer/MiniDrawer';

export const AppRouter = () => {

    const dispatch = useDispatch();

    const auth = useSelector(state => state.auth);

    const user = JSON.parse(localStorage.getItem('user'));

    const [checking, setChecking] = useState(true);

    const [isAuth, setIsAuth] = useState(false);

    useEffect(() => {
        if(auth?.token || user?.token)
            setIsAuth(true);
        
        else
            setIsAuth(false);

        setChecking(false);

        
    }, [dispatch, setChecking, setIsAuth, auth]);


    if( checking ) {
        return (
            <h1>Wait...</h1>
        );
    } else 
    return (
        <Router>
            {/* Router path auth no exact component authRouter */}
            {/* main route exact path / component journalpage */}
            <div >
                <Switch>
                    <PublicRoute isAuth={isAuth} path='/auth' component={AuthRouter}/>
                    <PrivateRoute isAuth={isAuth} path='/' component={MiniDrawer}/>

                    <Redirect to='/auth/login'/>
                </Switch>
            </div>
            
        </Router>
    )
}
