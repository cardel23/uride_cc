# Uride Command Center
This is the development guide for the new Command Center web app made with React.js


## Run Locally

Clone the project

```bash
  git clone https://bitbucket.org/cardel23/uride_cc
```

Go to the project directory

```bash
  cd uride_cc
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm start
```  
## Deployment to firebase

Create the build folder

```bash
  npm run build
```
Deploy to firebase hosting
```bash
  firebase deploy --only hosting
```
## Environment Variables

For this prototype there is not an .env file, but has a firebase-config file that holds the variables for the firebase project

**Note: Please change the firebase-config to your own firebase project**
  
## Related

The date picker for the reliability report is a third party open source component. To check references, visit [react-date-range](https://hypeserver.github.io/react-date-range)
  
## Authors

- [Carlos Delgadillo](https://bitbucket.org/cardel23/)
